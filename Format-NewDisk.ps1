function Format-NewDisk {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $false,Position = 0)]
        [string] $FileSystemLabel = 'Windows2Go',
        [Parameter(Mandatory = $false,Position = 1)]
        [ValidateSet('MBR','GPT')]
        [string] $PartitionStyle = 'GPT'
    ) DynamicParam {
        $ParameterName = 'DiskToFormat'
        $RuntimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        $AttributeCollection        = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
        $ParameterAttribute         = New-Object System.Management.Automation.ParameterAttribute
        $ParameterAttribute.Mandatory = $false
        $ParameterAttribute.Position = 3
        $AttributeCollection.Add($ParameterAttribute) # Add the attributes to the attributes collection
        $RawDisks = Get-Disk | Where-Object PartitionStyle -eq 'RAW' | Select-Object -ExpandProperty FriendlyName
        $ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($RawDisks) # Generate and set the ValidateSet
        $AttributeCollection.Add($ValidateSetAttribute)
        $RuntimeParameter = New-Object System.Management.Automation.RuntimeDefinedParameter($ParameterName, [string], $AttributeCollection)
        $RuntimeParameterDictionary.Add($ParameterName, $RuntimeParameter)
        return $RuntimeParameterDictionary
    }
    begin {

    }
    process {
        $FormatVolumeArguments = @{
            FileSystem         = 'NTFS'
            NewFileSystemLabel = $FileSystemLabel
            Confirm            = $false
        }
        $DiskObject = Get-Disk -FriendlyName $PSBoundParameters.$ParameterName
        $DiskSize =  [double]('{0:N2}' -f ($DiskObject.Size / 1GB))
        Write-Host -Object "Going to format disk:"
        Write-Host -Object "$($DiskObject.FriendlyName)"
        Write-Host -Object "$DiskSize GiB"

        function Format-W2GDisk {
            $DiskObject |
            Initialize-Disk -PartitionStyle $PartitionStyle -PassThru |
            New-Partition -AssignDriveLetter -UseMaximumSize |
            Format-Volume @FormatVolumeArguments
        }
    }
    end {
    }
}
